from django.urls import path
from .views import*

app_name="quickapp"
urlpatterns=[


path('login/',LoginView.as_view(),name="login"),

path("logout/",LogoutView.as_view(),name="logout"),


#client
path('',ClientHomeView.as_view(),name="clienthome"),
path('contact/',ClientContactView.as_view(),name="clientcontact"),
path('about/',ClientAboutView.as_view(),name="clientabout"),
path('profession/',ClientProfession.as_view(),name="clientprofession"),
path('events/',ClientEventView.as_view(),name="clientevent"),
path('employerregister/', EregisterView.as_view(),name="employerregister"),
path('jobseekerregister/', JregisterView.as_view(),name="jobseekerregister"),
path('username-checker/', IdCheckerView.as_view(),name="idchecker"),
path('job/<int:pk>/detail', ClientJobDetailView.as_view(),name="jobdetail"),
path("results/", ClientSearchView.as_view(), name="search"),
path("category/<int:pk>/list/", ClientCategoryDetailView.as_view(), name="clientcategorydetail"),





#admin
path("admin/home/",AdminHomeView.as_view(),name="adminhome"),
path("admin/job/category/list/",AdminJobCategoryListView.as_view(),name="adminjobcategorylist"),
path("admin/jobcategory/<int:pk>/detail/",AdminJobCategoryDetailView.as_view(),name="adminjobcategorydetail"),
path("admin/jobcategory/create/",AdminJobCategoryCreateView.as_view(),name="adminjobcategorycreate"),
path("admin/jobcategory/<int:pk>/update/",AdminJobCategoryUpdateView.as_view(),name="adminjobcategoryupdate"),
path("admin/jobcategory/<int:pk>/delete/",AdminJobCategoryDeleteView.as_view(),name="adminjobcategorydelete"),
path("admin/job/list/",AdminJobListView.as_view(),name="adminjoblist"),
path("admin/job/<int:pk>/delete/",AdminJobDeleteView.as_view(),name="adminjobdelete"),
path("admin/job/<int:pk>/detail/",AdminJobDetailView.as_view(),name="adminjobdetail"),
#path("admin/job/category/list/",AdminJobCategoryListView.as_view(),name="adminjobcategorylist"),


path("admin/profile/",AdminProfileView.as_view(),name="adminprofile"),

path("admin/employer/list/",EmployerListView.as_view(),name="employerlist"),
path("admin/employers/list/",AdminEmployerListView.as_view(),name="adminemployerlist"),
path("admin/employer/<int:pk>/detail/",AdminEmployerDetailView.as_view(),name="adminemployerdetail"),
path("admin/employer/<int:pk>/delete/",AdminEmployerDeleteView.as_view(),name="adminemployerdelete"),

path("admin/jobseeker/list/",AdminJobSeekerListView.as_view(),name="adminjobseekerlist"),
path("admin/jobseeker/<int:pk>/detail",AdminJobSeekerDetailView.as_view(),name="adminjobseekerdetail"),
path("admin/jobseeker/<int:pk>/delete/",AdminJobSeekerDeleteView.as_view(),name="adminjobseekerdelete"),


path("admin/slideshow/list/", AdminSlideShowListView.as_view(), name='adminslideshowlist'),
path("admin/slideshow/create/", AdminSlideShowCreateView.as_view(), name='adminslideshowcreate'),
path("admin/slideshow/<int:pk>/update/", AdminSlideShowUpdateView.as_view(), name='adminslideshowupdate'),
path("admin/slideshow/<int:pk>/delete/", AdminSlideShowDeleteView.as_view(), name='adminslideshowdelete'),


#employer
path("employer/home/",EmployerHomeView.as_view(),name="employerhome"),
path("employer/dashboard/",EmployerDashBoardView.as_view(),name="employerdashboard"),
path("employer/jobs/create/",EmployerJobCreateView.as_view(),name="employerjobcreate"),
path('employer/job/<int:pk>/delete/',EmployerJobDeleteView.as_view(), name="employerjobdelete"),
path('employer/job/<int:pk>/update/',EmployerJobUpdateView.as_view(), name="employerjobupdate"),
path("employer/jobs/<int:pk>/detail/",EmployerJobDetailView.as_view(),name="employerjobdetail"),
# path("employer/jobs/<int:pk>/detail/",EmployerJobDetailView.as_view(),name="employerjobdetail"),
# path("employer/<int:pk>/detail/",EmployerOwnDetailView.as_view(),name="employerowndetail"),

path("employer/category/",EmployerCategoryView.as_view(),name="employercategory"),
path("employer/category/create/",EmployerCategoryCreateView.as_view(),name="employercategorycreate"),
path('employer/category/<int:pk>/delete/',EmployerCategoryDeleteView.as_view(), name="employercategorydelete"),
path('employer/category/<int:pk>/update/',EmployerCategoryUpdateView.as_view(), name="employercategoryupdate"),
path("employer/logout/",EmployerLogoutView.as_view(),name="employerlogout"),





path("search/",EmployerSearchView.as_view(),name="employersearch"),

path("employer/home/jobs/list/",EmployerHomeJobListView.as_view(),name="employerhomejoblist"),
path("employer/category/list/",EmployerCategoryListView.as_view(),name="employercategorylist"),
path("employer/category/<int:pk>/detail/",EmployerCategoryDetailView.as_view(),name="employercategorydetail"),
path("employer/profile/<int:pk>/detail/",EmployerprofileDetailView.as_view(),name="employerprofile"),
path("employer/jobseeker/<int:pk>/detail/",EmployerJobseekerDetailView.as_view(),name="employerjobseeker"),
path("jobseeker/jobseekerapplied/", JobSeekerJobappliedmessagelView.as_view(), name="jobmessage"),











# jobseeker
path("jobseeker/home/",JobSeekerHomeView.as_view(),name="jobseekerhome"),
path("jobseeker/profile/list/",JobSeekerProfileListView.as_view(),name="jobseekerprofilelist"),

path("jobseeker/profile/detail/",JobSeekerProfileDetailView.as_view(),name="jobseekerprofiledetail"),
path("profile/detail/",ProfileDetailView.as_view(),name="profiledetail"),
path("jobseeker/profile/<int:pk>/update/",JobSeekerProfileUpdateView.as_view(),name="jobseekerprofileupdate"),
path("jobseeker/profile/<int:pk>/delete/",JobSeekerProfileDeleteView.as_view(),name="jobseekerprofiledelete"),


path("job/<int:pk>/apply",JobSeekerJobApplyView.as_view(),name="jobseekerjobapply"),


path("jobseeker/job/list",J_ApplyListView.as_view(),name="jobseekerjoblist"),
path("jobseeker/job/<int:pk>/detail",JobSeekerJobDetailView.as_view(),name="jobseekerjobdetail"),

#contact
path("contact/create/",ContactCreateView.as_view(),name="contactcreate"),
path("contact/list/",ContactListView.as_view(),name="contactlist"),
path("contact/<int:pk>/update/",ContactUpdateView.as_view(),name="contactupdate"),


#about
path("about/create/",AboutCreateView.as_view(),name="aboutcreate"),
path("about/list/",AboutListView.as_view(),name="aboutlist"),
path("about/<int:pk>/update/",AboutUpdateView.as_view(),name="aboutupdate"),




]