from django import forms
from .models import*


class LoginForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	password=forms.CharField(widget=forms.PasswordInput())


class SlideShowForm(forms.ModelForm):
    class Meta:
        model = SlideShow
        fields = ['title', 'caption', 'image']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'caption': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Main Text here'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control'
            }),
        }


class JregisterForm(forms.ModelForm):

    username = forms.CharField(widget=forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'username',
                }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'password'
                }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'confirm password'
                }))

    class Meta:
        model = JobSeeker
        fields = ["username", "password", "confirm_password","skill", "full_name",
                  "mobile", "email", "cv", "address", "image", "dob", "education"]
        widgets = {
            'full_name': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Full Name'
            }),
            'skill': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Skill'
            }),
            'address': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Address'
            }),
            'dob': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Dob'
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Mobile Number'
            }),
            'image': forms.FileInput(attrs={
                'class': 'login-form-rec'
            }),
            'email': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Email'
            }),
            'education': forms.Select(attrs={
                'class': 'frm-field required',
                'id' : 'country13',
                'placeholder': 'Education'
            }),
            'cv' : forms.FileInput(attrs={
            	'class': 'login-form-rec',
            	'placeholder': 'cv'
            	}),
   
        }
    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError("This username is already taken")
        return uname



class EregisterForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'login-form-rec',
        'placeholder': 'username'
        }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'login-form-rec',
        'placeholder': 'password'
        }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'login-form-rec',
        'placeholder': 'confirm password'
        }))

    class Meta:
        model = Employer
        fields = ["username", "password", "confirm_password","company_name", "full_name",
                  "company_logo", "company_photo", "mobile", "email", "website", "address",
                  "image", "location", "estb_date","about"]
        widgets = {
            'full_name': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Owner Full Name'
            }),
            'company_name': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'company name'
            }),
            'address': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Address'
            }),
            'company_logo': forms.FileInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Company Logo'
            }),
            'company_photo': forms.FileInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Company Photo'
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Mobile Number'
            }),
            'phone': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Phone Number'
            }),
            'image': forms.FileInput(attrs={
                'class': 'login-form-rec'
            }),
            'email': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Email'
            }),
            'website': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Company website'
            }),
            'location': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Campany Location'
            }),
            'estb_date': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'Statblished Date'
            }),
            'about' : forms.Textarea(attrs={
            	'class': 'login-form-rec',
            	'placeholder': 'About'
            	}),   
        }

    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError("This username is already taken")

        return uname

class AdminJobCategoryForm(forms.ModelForm):
	class Meta:
		model=Category
		fields=["title","image"]


# project done by susan dulal
class EmployerJobForm(forms.ModelForm):
    class Meta:
        model = Job
        exclude = ["employer"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'




class EmployerCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields=["title","image",]






# Rupak

class JobApplyForm(forms.ModelForm):
	class Meta:
		model=Application
		fields=["cover_letter"]

class ContactForm(forms.ModelForm):
    class Meta:
        model=Contact
        fields = ["email","telephone","image","location"]

        widgets = {
            'telephone': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'telephone number'
            }),

            'email': forms.EmailInput(attrs={
            'class': 'login-form-rec',
            'placeholder': 'eg:abc@gmail.com'
            }),

            'image': forms.FileInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'image '
            }),

            'location': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'location'
            }),
        }

class AboutForm(forms.ModelForm):
    class Meta:
        model = About
        fields = ["detail","image"]

        widgets = {
            'detail': forms.TextInput(attrs={
                'class': 'login-form-rec',
                'placeholder': 'detail'
            }),
            'image': forms.FileInput(attrs={
                'class': 'login-form-rec'
                }),
            }




