# Generated by Django 2.2.4 on 2019-08-25 17:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quickapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=300)),
                ('image', models.ImageField(upload_to='category')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Employer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('full_name', models.CharField(max_length=200)),
                ('company_name', models.CharField(max_length=200)),
                ('company_logo', models.ImageField(upload_to='employer')),
                ('company_photo', models.ImageField(upload_to='employer')),
                ('image', models.ImageField(upload_to='employer')),
                ('phone', models.CharField(blank=True, max_length=200, null=True)),
                ('mobile', models.CharField(max_length=200)),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
                ('email', models.EmailField(max_length=254)),
                ('website', models.CharField(max_length=200)),
                ('estb_date', models.DateField()),
                ('about', models.TextField()),
                ('location', models.CharField(max_length=20)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='JobSeeker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('full_name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('mobile', models.CharField(max_length=200)),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
                ('cv', models.FileField(upload_to='Jobseeker')),
                ('skill', models.TextField()),
                ('image', models.ImageField(upload_to='Jobseeker')),
                ('dob', models.DateField()),
                ('education', models.CharField(choices=[('Intermediate', 'Intermediate'), ('Bachelor', 'Bachelor'), ('Master', 'Master')], max_length=500)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=300)),
                ('image', models.ImageField(upload_to='job')),
                ('no_of_post', models.PositiveIntegerField()),
                ('deadline', models.DateField()),
                ('level', models.CharField(choices=[('Internship', 'Internship'), ('Entry', 'Entry'), ('Mid', 'Mid'), ('Senior', 'Senior')], max_length=50)),
                ('experience', models.CharField(max_length=100)),
                ('salary', models.CharField(max_length=30)),
                ('description', models.TextField()),
                ('view_count', models.PositiveIntegerField(default=1)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quickapp.Category')),
                ('employer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quickapp.Employer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('cover_letter', models.FileField(upload_to='application')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quickapp.Job')),
                ('jobseeker', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quickapp.JobSeeker')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
