from django.shortcuts import render,redirect
from django.views.generic import*
from .models import*
from .forms import*
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.db.models import Q # search ma or logic lagauna lai import gareko
from django.core.mail import send_mail
from django.conf import settings




class BaseMixin(object):  #BaseMixin afai ma view haina tara yesko property arko class ma inherite garana sakinxa
	def get_context_data(self,**kwargs):     #html ma View bata data pathauna get_context_data use garinxa
		context=super().get_context_data(**kwargs)
		context["categorylist"]=Category.objects.all()
		context["latestjobs"]=Job.objects.all().order_by("-id")
		context["pjobs"]=Job.objects.filter(job_tag="paid").order_by("-id")[0:4]
		context["featuredjobs"]=Job.objects.filter(job_tag="FeaturedJobs").order_by("-id")
		context["slideshows"]=SlideShow.objects.all()


		return context



class IdCheckerView(View):
    def get(self, request):
        uname = request.GET.get("username")
        if User.objects.filter(username=uname).exists():
            message = "User Name Not Available"
            color = "red"
        else:
            message = "User Name Available"
            color = "green"
        return JsonResponse({
            "message": message,
            "color": color,
        })


class AdminRequiredMixin(object):
	def dispatch(self,request,*args,**kwargs):
		user=request.user
		if user.is_authenticated and user.groups.filter(name="Admin").exists():
			pass
		else:
			return redirect("/login/")

		return super().dispatch(request,*args,**kwargs)




class EmployerRequiredMixin(object):
	def dispatch(self,request,*args,**kwargs):       #dispatch fuction is used for only private login  
		user=request.user
		if user.is_authenticated and user.groups.filter(name="Employer").exists():
			pass
		else:
			return redirect("/login/")


		return super().dispatch(request,*args,**kwargs)




class JobSeekerRequiredMixin(object):
	def dispatch(self,request,*args,**kwargs):
		user=request.user
		if user.is_authenticated and user.groups.filter(name="JobSeeker").exists():
			pass
		else:
			return redirect("/login/")

		return super().dispatch(request,*args,**kwargs)


class LogoutView(View):
	def get(self,request):
		logout(request)
		return redirect("/")


class LoginView(BaseMixin, FormView):
    template_name = 'clienttemplates/clientlogin.html'
    form_class = LoginForm
    success_url = "/"

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        usr = authenticate(username=uname, password=pword)

        #self.user = usr
        if usr is not None:
            login(self.request, usr)

        else:
            return render(self.request, self.template_name,
                          {
                              "error": "Incorrect Username or Password",
                              "form": form
                          })

        return super().form_valid(form)

    def get_success_url(self):
        user = self.request.user
        if user.groups.filter(name="Employer").exists():
            return "/employer/home/"

        elif user.groups.filter(name="JobSeeker").exists():
            return "/"

        elif user.groups.filter(name="Admin").exists():
            return "/admin/home/"

        else:
            return "/login/"


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/")


class ClientSearchView(BaseMixin, TemplateView):
    template_name = "clienttemplates/clientsearch.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        keyword = self.request.GET['searchkey']


        companies = Employer.objects.filter(company_name__icontains=keyword)
        context['companies'] = companies

        filter_condition = Q(title__icontains=keyword) \
            | Q(description__icontains=keyword) \
            | Q(employer__company_name__icontains=keyword)
        results = Job.objects.filter(filter_condition)
        context['searched_jobs'] = results

        return context


class ClientHomeView(BaseMixin, ListView):
	template_name="clienttemplates/clienthome.html"
	queryset = Job.objects.all().order_by("-created_at")[0:8]
	context_object_name = "joblists"


class ClientJobDetailView(BaseMixin, DetailView):
	template_name="clienttemplates/clientjobdetails.html"
	queryset = Job.objects.all()
	context_object_name = "jobD"


class ClientContactView(TemplateView):
	template_name="clienttemplates/clientcontact.html"


class ClientAboutView(TemplateView):
	template_name="clienttemplates/clientabout.html"


class ClientProfession(TemplateView):
	template_name="clienttemplates/clientprofession.html"

class ClientEventView(TemplateView):
	template_name="clienttemplates/clientevent.html"


class ClientCategoryDetailView(BaseMixin, DetailView):
    template_name = "clienttemplates/clientcategorydetail.html"
    model = Category
    context_object_name = "categoryobject"


class EregisterView(CreateView):
	template_name = "clienttemplates/clientemployerregister.html"
	form_class = EregisterForm
	success_url = reverse_lazy('quickapp:employerhome')

	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		pword = form.cleaned_data["password"]

		user = User.objects.create_user(uname, '', pword)
		form.instance.user = user
		login(self.request, user)

		return super().form_valid(form)


class JregisterView(CreateView):
	template_name = "clienttemplates/clientjobseekerregister.html"
	form_class = JregisterForm
	success_url = reverse_lazy('quickapp:profiledetail')


	def form_valid(self, form):  # to handle data of form
		uname = form.cleaned_data["username"]
		pword = form.cleaned_data["password"]

		user = User.objects.create_user(uname, '', pword)
		form.instance.user = user
		login(self.request, user)

		return super().form_valid(form)





	#admin view started
class AdminHomeView(AdminRequiredMixin,TemplateView):
	template_name="admintemplates/adminhome.html"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context["allcats"]=Category.objects.all()


		return context


class AdminJobListView(AdminRequiredMixin, ListView):
	login_url = '/login/'
	template_name = 'admintemplates/adminjoblist.html'
	queryset = Job.objects.all()
	context_object_name = 'adminjoblists'



class AdminJobDeleteView(AdminRequiredMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/Adminjobdelete.html'
    model = Job
    success_url = '/admin/job/list/'


class AdminJobDetailView(AdminRequiredMixin, DetailView):
	template_name="admintemplates/adminjobdetail.html"
	model=Job
	context_object_name="adminjobdetail"


#slide show by prvin rai
class AdminSlideShowListView(AdminRequiredMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowlist.html'
    queryset = SlideShow.objects.all()
    context_object_name = 'slideshows'


class AdminSlideShowCreateView(AdminRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowcreate.html'
    form_class = SlideShowForm
    success_url = '/admin/slideshow/list/'


class AdminSlideShowUpdateView(AdminRequiredMixin, UpdateView):
    login_url = '/login/'
    template_name = '/admintemplates/adminslideshowcreate.html'
    form_class = SlideShowForm
    model = SlideShow
    success_url = 'admin/slideshow/list/'


class AdminSlideShowDeleteView(AdminRequiredMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowdelete.html'
    model = SlideShow
    success_url = '/admin/slideshow/list/'


class AdminJobCategoryListView(AdminRequiredMixin,ListView):
	template_name="admintemplates/adminjobcategorylist.html"
	queryset=Category.objects.all()
	context_object_name="allcategory"




class AdminJobCategoryDetailView(AdminRequiredMixin,DetailView):
	template_name="admintemplates/adminjobcategorydetail.html"
	model=Category
	context_object_name="categorydetail"



class AdminJobCategoryCreateView(AdminRequiredMixin,CreateView):
	template_name="admintemplates/adminjobcategorycreate.html"
	form_class=AdminJobCategoryForm
	success_url= reverse_lazy("quickapp:adminjobcategorylist")




class AdminJobCategoryUpdateView(AdminRequiredMixin,UpdateView):
	template_name="admintemplates/adminjobcategorycreate.html"
	model=Category
	form_class=AdminJobCategoryForm
	success_url= reverse_lazy("quickapp:adminjobcategorylist")




class AdminJobCategoryDeleteView(AdminRequiredMixin,DeleteView):
	template_name="admintemplates/adminjobcategorydelete.html"
	model=Category
	success_url= reverse_lazy("quickapp:adminjobcategorylist")



class AdminProfileView(AdminRequiredMixin,TemplateView):
	template_name="admintemplates/adminprofile.html"



class AdminEmployerListView(AdminRequiredMixin,ListView):
	template_name="admintemplates/adminemployerlist.html"
	queryset=Employer.objects.all()
	context_object_name="adminemployerlists"

class AdminEmployerDetailView(AdminRequiredMixin,DetailView):
	template_name="admintemplates/adminemployerdetail.html"
	model=Employer
	context_object_name='adminempdetail'


class AdminEmployerDeleteView(AdminRequiredMixin,DeleteView):
	template_name="admintemplates/adminemployerdelete.html"
	model=Employer
	success_url= reverse_lazy("quickapp:adminemployerlist")



class AdminJobSeekerListView(AdminRequiredMixin,ListView):
	template_name="admintemplates/adminjobseekerlist.html"
	queryset=Employer.objects.all()
	context_object_name="alljbs"

class AdminJobSeekerDetailView(AdminRequiredMixin,DetailView):
	template_name="admintemplates/adminjobseekerdetail.html"
	model=JobSeeker
	context_object_name="adminjbsdetail"

class AdminJobSeekerDeleteView(AdminRequiredMixin,DeleteView):
	template_name="admintemplates/adminjobseekerdelete.html"
	model=JobSeeker
	success_url=reverse_lazy("quickapp:adminjobseekerlist")




# emp pannel started

class EmployerListView(EmployerRequiredMixin,TemplateView):
	template_name="admintemplates/employerlist.html"



	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		user=self.request.user  #give loggedin user
		#company=Employer.objects.get(user=user)  #get function return only one data
		emps=Employer.objects.all() #give list of job which oploaded by this employer
		#context["full_name"]=company #html page ma employer.company_name lekhekole employer lekheko
		context["allemps"]=emps


		return context


class EmployerDetailView(EmployerRequiredMixin,DetailView):
	template_name="admintemplates/employerdetail.html"
	model=Employer
	context_object_name="empdetail"


class EmployerDeleteView(EmployerRequiredMixin,DeleteView):
	template_name="admintemplates/employerdelete.html"
	model=Employer
	success_url= reverse_lazy("quickapp:employerlist")


class JobseekerListView(JobSeekerRequiredMixin,TemplateView):
	template_name="admintemplates/jobseekerlist.html"



	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		user=self.request.user  #give loggedin user
		# company=JobSeeker.objects.get(user=user)  #get function return only one data
		jbs=JobSeeker.objects.all() #give list of job which oploaded by this employer
		# context["jobseeker"]=company #html page ma employer.company_name lekhekole employer lekheko
		context["alljbs"]=jbs


		return context

class JobseekerDetailView(JobSeekerRequiredMixin,DetailView):
	template_name="admintemplates/jobseekerdetail.html"
	model=JobSeeker
	context_object_name="jbsdetail"


class JobseekerDeleteView(JobSeekerRequiredMixin,DeleteView):
	template_name="admintemplates/jobseekerdelete.html"
	model=JobSeeker
	success_url=reverse_lazy("quickapp:jobseekerlist")



# project done by susan dulal


class EmployerHomeView(TemplateView):
	template_name="employertemplates/employerhome.html"
	# this is for login security
	# def dispatch(self,request,*args,**kwargs):
	# 	user=request.user

	# 	if user.is_authenticated and user.groups.filter(name="employ").exists():
	# 		pass
	# 	else:
	# 		return redirect("/login/")
	# 	return super().dispatch(request,*args,**kwargs)<!__this is now inherited from above class__!>




class EmployerCategoryView(EmployerRequiredMixin, ListView):
	template_name="employertemplates/employercategory.html"
	model=Category
	context_object_name="ccl"
	





class EmployerCategoryCreateView(EmployerRequiredMixin, CreateView):
	template_name="employertemplates/employercategorycreate.html"
	form_class=EmployerCategoryForm
	success_url="/employer/category/"
	def dispatch(self,request,*args,**kwargs):
		user=request.user
		if user.is_authenticated and user.groups.filter(name="employ").exists():
			pass
		else:
			return redirect("/login/")
		return super().dispatch(request,*args,**kwargs)#<!__this is now inherited from above class__!>





class EmployerCategoryDeleteView(EmployerRequiredMixin, DeleteView):
	template_name="employertemplates/employercategorydelete.html"
	model = Category
	success_url="/employer/category/"




class EmployerCategoryUpdateView(EmployerRequiredMixin, UpdateView):
	template_name="employertemplates/employercategoryupdate.html"
	model = Category
	form_class=EmployerCategoryForm
	success_url="/employer/category/"


	

class EmployerprofileDetailView(EmployerRequiredMixin, DetailView):
	template_name="employertemplates/employerdetail.html"
	model = Employer
	context_object_name="empdetail"



class EmployerDashBoardView(EmployerRequiredMixin, TemplateView):
	template_name="employertemplates/employerdashboard.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		company = Employer.objects.get(user=user)
		myjobs = Job.objects.filter(employer=company)
		context["empjl"] = myjobs

		return context
	# def dispatch(self,request,*args,**kwargs):
	# 	user=request.user
	# 	if user.is_authenticated and user.groups.filter(name="employ").exists():
	# 		pass
	# 	else:
	# 		return redirect("/login/")
	# 	return super().dispatch(request,*args,**kwargs)

	
# class EmployerOwnDetailView(DetailView):
# 	template_name="employertemplates/employerowndetail.html"
# 	model=Employer
# 	context_object_name="empdeil"


class EmployerJobCreateView(CreateView):
	template_name="employertemplates/employerjobcreate.html"

	form_class=EmployerJobForm
	success_url="/employer/dashboard/"
	# def dispatch(self,request,*args,**kwargs):
	# 	user=request.user
	# 	if user.is_authenticated and user.groups.filter(name="employ").exists():
	# 		pass
	# 	else:
	# 		return redirect("/login/")
	# 	return super().dispatch(request,*args,**kwargs)<!__this is now inherited from above class__!>
	def form_valid(self,form):
		usr=self.request.user

		emplr=Employer.objects.get(user=usr)
		form.instance.employer=emplr



		return super().form_valid(form)


class EmployerJobDeleteView(DeleteView):
	template_name="employertemplates/employerjobdelete.html"
	model = Job
	success_url="/employer/dashboard/"



class EmployerJobUpdateView(UpdateView):
	template_name="employertemplates/employerjobupdate.html"
	model = Job
	form_class=EmployerJobForm
	success_url="/employer/dashboard/"


class EmployerJobDetailView(DetailView):
	template_name="employertemplates/employerjobdetail.html"
	model=Job
	context_object_name="jobdetail"

	# def dispatch(self,request,*args,**kwargs):
	# 	user=request.user
	# 	if user.is_authenticated and user.groups.filter(name="employ").exists():
	# 		pass
	# 	else:
	# 		return redirect("/login/")<!__this is now inherited from above class__!>



	# 	return super().dispatch(request,*args,**kwargs)



class EmployerLogoutView(View):
	def get(self,request):
		logout(request)
		return redirect("/")






class EmployerSearchView(TemplateView):
	template_name="employertemplates/employersearch.html"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		Kw=self.request.GET.get("keyword")
		# getting datas from server

		# results=Job.objects.filter(title__icontains=Kw)

	





		results=Job.objects.filter(Q(title__icontains=Kw)|
			                       Q(description__icontains=Kw)|
			                       Q(category__title__icontains=Kw)|
			                       Q(employer__company_name__icontains=Kw))


		# results = Job.objects.*

		# print(results)
		
		# company name batwa search garna



		context["search_results"]=results
		# data sending to html page


		return context





class EmployerHomeJobListView(EmployerRequiredMixin,ListView):
	template_name="employertemplates/employerhomepostedjob.html"
	queryset=Job.objects.all()
	context_object_name="employerhmjoblist"

class EmployerCategoryListView(EmployerRequiredMixin,ListView):
	template_name="employertemplates/employercategorylist.html"
	queryset=Category.objects.all()
	context_object_name="employercategorylist"


class EmployerCategoryDetailView(EmployerRequiredMixin,DetailView):
	template_name="employertemplates/employercategorydetail.html"
	model=Category
	context_object_name="categorydetail"


class EmployerJobseekerDetailView(EmployerRequiredMixin,DetailView):
	template_name="employertemplates/employerjobseekerdetail.html"
	model=JobSeeker
	context_object_name="empjobseekerdetail"



#jobseeker
class JobSeekerHomeView(JobSeekerRequiredMixin,TemplateView):
	template_name='jobseekertemplates/jobseekerhome.html'


class JobSeekerProfileListView(JobSeekerRequiredMixin,ListView):
	template_name='jobseekertemplates/jobseekerprofilelist.html'
	model=JobSeeker
	context_object_name='j_profilelist'


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		context["thisuser"]=user

		# company = Employer.objects.get(user=user)
		# myjobs = Job.objects.filter(employer=company)
		# context["empjl"] = myjobs

		return context









class JobSeekerProfileDetailView(JobSeekerRequiredMixin,TemplateView):
	template_name='jobseekertemplates/jobseekerprofiledetail.html'


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		thisuser = JobSeeker.objects.filter(user=user)
		context["jobseekerdetails"]=thisuser
		

		return context

class ProfileDetailView(JobSeekerRequiredMixin,TemplateView):
	template_name='clienttemplates/jobseekerprofiledetail.html'


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		thisuser = JobSeeker.objects.filter(user=user)
		context["jobseekerdetails"]=thisuser
		

		return context

class JobSeekerProfileUpdateView(JobSeekerRequiredMixin,UpdateView):
	template_name='clienttemplates/jobseekerprofileupdate.html'
	model=JobSeeker
	form_class=JregisterForm
	success_url=reverse_lazy("quickapp:profiledetail")

	
class JobSeekerProfileDeleteView(JobSeekerRequiredMixin,DeleteView):
	template_name='jobseekertemplates/jobseekerprofiledelete.html'
	model=JobSeeker
	success_url='/jobseeker/home/'



class JobSeekerJobApplyView(JobSeekerRequiredMixin,CreateView):
	template_name='clienttemplates/jobapplyform.html'
	form_class=JobApplyForm
	success_url='/jobseeker/jobseekerapplied/'

	def form_valid(self, form):  # keyword argument=>Kwargs
		job_id = self.kwargs["pk"]
		job = Job.objects.get(id=job_id)
		form.instance.job = job
		loggedin_user = self.request.user
		jobseeker = JobSeeker.objects.get(user=loggedin_user)
		form.instance.jobseeker = jobseeker

		return super().form_valid(form)

class J_ApplyListView(JobSeekerRequiredMixin,TemplateView):
	template_name='clienttemplates/jobseekerjoblist.html'


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		thisuser = JobSeeker.objects.get(user=user)
		application = Application.objects.filter(jobseeker=thisuser)
		context["applylist"]=application


		return context



class JobSeekerJobDetailView(JobSeekerRequiredMixin,DetailView):
	template_name='jobseekertemplates/appliedjobdetail.html'


class JobSeekerJobappliedmessagelView(TemplateView):
	template_name='clienttemplates/jobappliedmessage.html'


#end job seeker


#contact
class ContactCreateView(CreateView):
	template_name = 'admintemplates/contactcreate.html'
	form_class = ContactForm
	success_url= reverse_lazy("quickapp:adminhome")

class ContactListView(ListView): 
	template_name = 'admintemplates/clientcontact.html'
	model = Contact
	context_object_name="contactlist"

class ContactUpdateView(UpdateView):
	template_name = 'admintemplates/contactcreate.html'
	model = Contact
	form_class = ContactForm
	success_url = reverse_lazy("quickapp:adminhome")


#About
class AboutCreateView(CreateView):
	template_name = 'admintemplates/aboutcreate.html'
	form_class = AboutForm
	success_url = reverse_lazy("quickapp:adminhome")

class AboutListView(ListView):
	template_name = 'admintemplates/aboutlist.html'
	model = About
	context_object_name = 'aboutlist'

class AboutUpdateView(UpdateView):
	template_name = 'admintemplates/aboutcreate.html'
	model = About
	form_class = AboutForm
	success_url = reverse_lazy("quickapp:adminhome")








