from django.db import models
from django.contrib.auth.models import User,Group


class TimeStamp(models.Model):
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	class Meta:
		abstract=True




class Admin(TimeStamp):
	user=models.OneToOneField(User,on_delete=models.CASCADE)
	full_name=models.CharField(max_length=200)
	email=models.EmailField()
	mobile=models.CharField(max_length=200)
	image=models.ImageField(upload_to="admin")
	address=models.CharField(max_length=200,null=True,blank=True)

	def save(self,*args,**kwargs):
		group,created=Group.objects.get_or_create(name="Admin")
		self.user.groups.add(group)
		super().save(*args,**kwargs)

	def __str__(self):
		return self.full_name


class Employer(TimeStamp):
	user=models.OneToOneField(User,on_delete=models.CASCADE)
	full_name=models.CharField(max_length=200)
	company_name=models.CharField(max_length=200)
	company_logo=models.ImageField(upload_to="employer",null=True,blank=True)
	company_photo=models.ImageField(upload_to="employer",null=True,blank=True)
	image=models.ImageField(upload_to="employer",null=True,blank=True)
	phone=models.CharField(max_length=200,null=True,blank=True)
	mobile=models.CharField(max_length=200)
	address=models.CharField(max_length=200,null=True,blank=True)
	email=models.EmailField()
	website=models.CharField(max_length=200)
	estb_date=models.DateField(null=True,blank=True)
	about=models.TextField()
	location=models.CharField(max_length=20)

	def save(self,*args,**kwargs):
		empgroup,created=Group.objects.get_or_create(name="Employer")
		self.user.groups.add(empgroup)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.company_name


EDUCATION=(
	("Intermediate","Intermediate"),
	("Bachelor","Bachelor"),
	("Master","Master"),


	)
class JobSeeker(TimeStamp):
	user=models.OneToOneField(User,on_delete=models.CASCADE)
	full_name=models.CharField(max_length=200)
	email=models.EmailField()
	mobile=models.CharField(max_length=200)
	address=models.CharField(max_length=200,null=True,blank=True)
	cv=models.FileField(upload_to="Jobseeker" ,null=True,blank=True)
	skill=models.TextField()
	image=models.ImageField(upload_to="Jobseeker", null=True,blank=True)
	dob=models.DateField()
	education=models.CharField(max_length=500,choices=EDUCATION)

	def save(self,*args,**kwargs):
		group,created=Group.objects.get_or_create(name="JobSeeker")
		self.user.groups.add(group)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.full_name



class Category(TimeStamp):
	title=models.CharField(max_length=300)
	image=models.ImageField(upload_to="category")

	def __str__(self):
		return self.title


JOB_LEVEL=(
	("Internship","Internship"),
	("Entry","Entry"),
	("Mid","Mid"),
	("Senior","Senior"),
	)

JOB_TAG=(
	("HotJobs","Hot Jobs"),
	("TopJobs","Top Jobs"),
	("FeaturedJobs","Featured Jobs"),
	("NewspaperJobs","Newspaper Jobs"),
	("paid","Paid Jobs"),
	)


class Job(TimeStamp):
	title=models.CharField(max_length=300)
	category=models.ForeignKey(Category,on_delete=models.CASCADE)
	employer=models.ForeignKey(Employer,on_delete=models.CASCADE)
	image=models.ImageField(upload_to="job")
	no_of_post=models.PositiveIntegerField()
	deadline=models.DateField()
	level=models.CharField(max_length=50,choices=JOB_LEVEL)
	experience=models.CharField(max_length=100)
	salary=models.CharField(max_length=30)
	description=models.TextField()
	view_count=models.PositiveIntegerField(default=1)
	job_tag = models.CharField(max_length=50, choices=JOB_TAG, null=True, blank=True)

	def __str__(self):
		return self.title



class Application(TimeStamp):
	job=models.ForeignKey(Job,on_delete=models.CASCADE)
	jobseeker=models.ForeignKey(JobSeeker,on_delete=models.CASCADE)
	cover_letter=models.FileField(upload_to="application", null=True, blank=True)

	def __str__(self):
		return self.job.title


class SlideShow(TimeStamp):
    title = models.CharField(max_length=90)
    caption = models.CharField(max_length=90, blank=True, null=True)
    image = models.ImageField(upload_to='slideshows')

    def __str__(self):
        return self.title


class Contact(TimeStamp):
	telephone = models.CharField(max_length=100)
	email = models.EmailField()
	image = models.ImageField(upload_to='contact_image')
	location = models.CharField(max_length=200)

class About(TimeStamp):
	image = models.ImageField(upload_to='about_image')
	detail = models.TextField()











